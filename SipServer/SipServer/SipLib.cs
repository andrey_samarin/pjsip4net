﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Castle.Windsor;
using Common.Logging;
using pjsip.Interop;
using pjsip4net;
using pjsip4net.Calls;
using pjsip4net.Configuration;
using pjsip4net.Container.Castle;
using pjsip4net.Core;
using pjsip4net.Core.Configuration;
using pjsip4net.Core.Data;
using pjsip4net.Core.Data.Events;
using pjsip4net.Core.Interfaces.ApiProviders;
using pjsip4net.Core.Utils;
using pjsip4net.Interfaces;

namespace SipServer
{
    public class SipLib
    {
        //private static ILog _logger;
        static public bool IsRing { get; set; }
        static public bool IsVoice { get; set; }
        public string PhoneNumber { get; set; }
        public ISipUserAgent ua { get; set; }
        static public System.Media.SoundPlayer soundPlayer;
        private string OperatorStatus;

        public SipLib()
        {
            IsRing = false;
            IsVoice = false;
            PhoneNumber = "";
            ua = null;
            soundPlayer = new SoundPlayer();
        }

        ~SipLib()
        {
            if (ua != null)
            {
                //ua.AccountManager.DefaultAccount.Unregister();
                //ua.AccountManager.Accounts.Each()
                foreach (var ac in ua.AccountManager.Accounts)
                {
                    ac.Unregister();
                }
                ua.Destroy();
                ua = null;
            }
        }

        public string DecodePhoneNumber(string pUri, bool incoming = false)
        {
            string ret = pUri;
            if (incoming)
            {
                var idx = pUri.IndexOf("<sip:", StringComparison.Ordinal);
                if (idx > 0)
                {
                    var outNumber = pUri.Substring(0, idx - 1);
                    outNumber.Replace("\"", "");
                    ret = outNumber.Replace('"', ' ');
                }
            }
            else
            {
                int idx1 = pUri.IndexOf(':');
                int idx2 = pUri.IndexOf('@');

                if (idx1 < idx2)
                {
                    ret = pUri.Substring(idx1 + 1, idx2 - idx1 - 1);
                }
                else
                {
                    ret = "";
                }
            }

            return ret.Trim();
        }

        public void SoundRing()
        {
            soundPlayer.SoundLocation = Environment.CurrentDirectory + "\\ring.wav";
            soundPlayer.Load();
            soundPlayer.PlayLooping();
        }
        public void SoundBusy()
        {
            soundPlayer.SoundLocation = Environment.CurrentDirectory + "\\busy.wav";
            soundPlayer.Load();
            soundPlayer.Play();
        }

        public void SoundStop()
        {
            soundPlayer.Stop();
        }

        public event EventHandler eventIncomingCall;
        public event EventHandler eventDisconnected;
        public event EventHandler eventConfirmed;

        public void Connect(string p_user, string p_password, string p_server, string p_port) //5060
        {
            //_logger = LogManager.GetLogger("root");     //logging is purely an application facility, you can choose whatever you want to log with
            var container = new WindsorContainer();
            var cfg = Configure.Pjsip4Net().With_CastleContainer(container); //.FromConfig();
            cfg.With(x =>
            {
                x.Config.AutoAnswer = false;
                //x.LoggingConfig.LogMessages = true;
                //x.LoggingConfig.TraceAndDebug = true;
            });
            ua = cfg.Build().Start();   //build and start

            ua.ImManager.IncomingMessage += IncomingMessage;
            ua.CallManager.CallRedirected += CallRedirected;
            ua.CallManager.IncomingDtmfDigit += IncomingDtmfDigit;
            ua.ImManager.NatDetected += OnNatDetected;
            ua.CallManager.IncomingCall += CallManager_IncomingCall;
            ua.CallManager.Ring += CallManager_Ring;
            ua.CallManager.CallStateChanged += CallManager_CallStateChanged;
            ua.AccountManager.Register(x =>
            {
                x.WithExtension(p_user);
                x.At(p_server);
                x.WithPassword(p_password);
                x.Through(p_port);
                return x.Default().Register();
            });
        }

        public void Disconnect()
        {
            if (ua == null)
                return;

            foreach (var ac in ua.AccountManager.Accounts)
            {
                ac.Unregister();
            }

            ua.Destroy();
            //Thread.Sleep(2000);
            ua = null;
        }

        private static void CallManager_Ring(object sender, RingEventArgs e)
        {
            if (e.RingOn && !e.IsRingback)
            {
                //soundPlayer.SoundLocation = Environment.CurrentDirectory + '';
                //soundPlayer.Load();
                Program.sipLib.SoundRing();
                //wplayer.controls.play();
                //System.Media.SystemSounds.Asterisk.Play();
            }
            else
            {
                //Program.sipLib.soundBusy();
                Program.sipLib.SoundStop();
                //System.Media.SoundPlayer sp = new System.Media.SoundPlayer();
                //sp.PlayLooping();
                //sp.Stop();
                //wplayer.controls.stop();
            }

            /*
            System.Console.WriteLine("Call {0} ring event.", e.CallId);
            var action = e.RingOn ? "turn on" : "switch off";
            var toneType = e.IsRingback ? "a ringback tone" : "a ring sound";
            System.Console.WriteLine("========>>> {0} {1}", action, toneType);
            System.Console.WriteLine("call={0}", (sender as ICallManager).Calls.Where(x => x.Id == e.CallId).First().RemoteContact);
            */
        }

        private void CallManager_CallStateChanged(object sender, CallStateChangedEventArgs e)
        {
            string phone;
            //System.Console.WriteLine("Call to {0}: {1}: {2}: {3}: {4}", e.DestinationUri, e.Duration, e.Id, e.InviteState, e.MediaState);
            switch (e.InviteState)
            {
                case InviteState.Calling:
                    phone = this.DecodePhoneNumber(e.DestinationUri);
                    if (phone.Length < 5)
                    {
                        this.SoundRing();
                    }
                    //soundPlayer.PlayLooping();
                    //IsRing = false;
                    //eventDisconnected(sender, null);
                    break;
                case InviteState.Confirmed:
                    IsVoice = true;
                    eventConfirmed(sender, null);
                    break;
                case InviteState.Disconnected:
                    if (IsVoice)
                    {
                        this.SoundStop();
                    }
                    else
                    {
                        this.SoundBusy();
                    }
                    //soundPlayer.Stop();
                    IsRing = false;
                    eventDisconnected(sender, null);
                    break;
            }
        }

        private void CallManager_IncomingCall(object sender, EventArgs<ICall> e)
        {
            IsRing = true;

            PhoneNumber = this.DecodePhoneNumber(e.Data.RemoteContact, true);

            // PSN, создаем новый сеанс в USD, пытаемся найти карточку клиента и открыть ее
            if (!String.IsNullOrEmpty(PhoneNumber))
            {
                MessageBox.Show($"Звонок с номера: {PhoneNumber}");
                //var typeOperation = "phonecall";
                //// !!!!!!!!!!!!!!!!!!!!!!!!!!
                //var callFrom = "123";   // временно... надо подставить номер, на который звонили
                //string url = $"http://localhost:5000?ani={PhoneNumber}&dnis={callFrom}&type={typeOperation}&phonenumber={PhoneNumber}";
                //using (WebClient client = new WebClient())
                //{
                //    client.DownloadString(url);
                //}
            }
            // PSN

            /*
            e.Data.Answer(true);
            System.Console.WriteLine("Incoming call from: {0}", e.Data.RemoteContact);
            */
            if (eventIncomingCall != null)
                eventIncomingCall(sender, null);
        }

        private static void OnNatDetected(object s, NatEventArgs ea)
        {
            /*
            System.Console.WriteLine("NAT type detection complete: {0}; {1}", ea.NatTypeName, ea.StatusText);
            */
        }

        private static void IncomingDtmfDigit(object sender, DtmfEventArgs eventArgs)
        {
            /*
            System.Console.WriteLine("Call {0} received {1} digits", eventArgs.CallId, eventArgs.Digit);
            */
        }

        private static void IncomingMessage(object sender, PagerEventArgs e)
        {
            /*
            System.Console.WriteLine("Message from " + e.From + ", text: " + e.Body);
            */
        }

        private static void CallRedirected(object sender, CallRedirectedEventArgs e)
        {
            e.Option = RedirectOption.Accept;
        }

        public string getAvailPlaybackDevices()
        {
            string ret = "";

            ua.MediaManager.PlaybackDevices.Each(x =>
            {
                ret += (ret.Length != 0) ? ";" : "";
                ret += x.Id + ";" + x.Name;
            });

            return ret;
        }
        public DeviceInfoResponse GetAvailPlaybackDevices()
        {
            var ret = new DeviceInfoResponse();

            try
            {
                var container = new WindsorContainer();
                var cfg = Configure.Pjsip4Net().With_CastleContainer(container);
                cfg.With(x =>
                {
                    x.Config.AutoAnswer = false;
                });
                ua = cfg.Build();   //build and start

                //var configure = Configure.Pjsip4Net();
                //configure.With(x =>
                //{
                //    x.Config.AutoAnswer = false;
                //});
                //ua = BuildUserAgent.Build(configure);
                if (ua != null)
                {
                    ua.Start();
                    var pd = ua.MediaManager.PlaybackDevices;
                    var cd = ua.MediaManager.CaptureDevices;
                    ret.PlayBackDevice = pd;
                    ret.CaptureBackDevice = cd;
                    ret.ResultOk = 1;
                }

                if (ua != null)
                {
                    ua.Destroy();
                    ua = null;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return ret;
        }

        public string getAvailCaptureDevices()
        {
            string ret = "";

            ua.MediaManager.CaptureDevices.Each(x =>
            {
                ret += (ret.Length != 0) ? ";" : "";
                ret += x.Id.ToString() + ";" + x.Name;
            });

            return ret;
        }

        public void setSoundDevices(int p_playbackDeviceNum, int p_captureDeviceNum)
        {
            if (ua != null)
            {
                ua.MediaManager.SetSoundDevices(p_playbackDeviceNum, p_captureDeviceNum);
            }
        }

        public bool Answer()
        {
            bool ret = false;

            if (ua != null)
            {
                /*
                pjsip4net.Interfaces.ICall call;
                if (ua.CallManager.Calls.Count > 0)
                {
                    call = ua.CallManager.Calls[0];
                }
                */

                //ua.CallManager.Calls.Where(x => x.InviteState == InviteState.Calling).Each(x =>
                ua.CallManager.Calls.Where(x => x.IsActive && ((x.InviteState == InviteState.Calling) || (x.InviteState == InviteState.None))).Each(x =>
                {
                    if (IsRing)
                    {
                        this.SoundStop();   // PSN
                        IsRing = false;       // PSN
                        IsVoice = true;
                    }
                    x.Answer(true);
                    ret = true;
                });
                //ua.CallManager.Calls.Where(x => x.InviteState == InviteState.Calling || x.InviteState == InviteState.Connecting || x.InviteState == InviteState.Incoming).Each(x => x.Answer(true));
                /*
                ua.CallManager.Calls.Where(x => 1 == 1).Each(x =>
                {
                    PhoneNumber = string.Format("{0}", x.InviteState);
                    x.Answer(true);
                });
                */
                //ua.CallManager.Calls.Each(x => x.Answer(true));
            }

            return ret;
        }

        public int Call(string phone)
        {
            int callId = 0;

            if (ua != null && !IsRing)
            {
                var account = ua.AccountManager.Accounts[0];
                string destUri = string.Format("sip:{0}@{1}:{2}", phone, account.RegistrarDomain, account.RegistrarPort);
                //var call = ua.CallManager.MakeCall(account, destUri);
                ICall call = ua.CallManager.MakeCall(destUri);
                callId = call.Id;
            }

            return callId;
        }

        private static void HangupForeground(object data)
        {
            pjsip4net.Interfaces.ICall call = (pjsip4net.Interfaces.ICall)data;
            call.Hangup();
        }

        public void Hangup()
        {
            if (ua != null && ua.CallManager != null)
            {
                if (IsRing)
                {
                    this.SoundBusy();
                }

                ua.CallManager.Calls.Where(x => x.IsActive && (
                    (x.InviteState == InviteState.Calling)
                    || (x.InviteState == InviteState.None)
                    || (x.InviteState == InviteState.Confirmed)
                    //|| (x.InviteState == InviteState.Incoming)
                    )).Each(x =>
                {
                    //Thread newThread = new Thread(HangupForeground);
                    //newThread.Start(x);
                    x.Hangup(string.Empty);
                });
                //ua.CallManager.HangupAll();
                //ua.CallManager.Calls[0].Hangup();
                foreach (var ac in ua.AccountManager.Accounts)
                {
                    ac.RenewRegistration();
                }
            }
        }

        //GDA001, maxy@columbusglobal.com, 24.05.2018
        // PSN - _callIdStr мне неоткуда взять... CTI панель ничего не знает про звонок

        //public int Transfer(string _callIdStr, string _phone)
        //{
        //    int resultOk = 0;
            
        //    if (ua != null)
        //    {
        //        var account = ua.AccountManager.Accounts[0];
        //        string destUri = string.Format("sip:{0}@{1}:{2}", _phone, account.RegistrarDomain, account.RegistrarPort);

        //        ua.CallManager.GetCallById(int.Parse(_callIdStr)).Transfer(destUri);
                
        //        resultOk = 1;
        //    }

        //    return resultOk;
        //}

        public int Transfer(string phone)
        {
            int resultOk = 0;

            if (ua != null)
            {
                var account = ua.AccountManager.Accounts[0];
                string destUri = $"sip:{phone}@{account.RegistrarDomain}:{account.RegistrarPort}";

                foreach (var call in ua.CallManager.Calls.Where(x => x.IsActive && (
                                                                      x.InviteState == InviteState.Calling
                                                                      || x.InviteState == InviteState.None
                                                                      || x.InviteState == InviteState.Confirmed
                                                                      || x.IsIncoming)
                )) call.Transfer(destUri);
                resultOk = 1;
            }

            return resultOk;
        }

        //GDA001, maxy@columbusglobal.com, 24.05.2018
        public int SetOperatorStatus(string _status)
        {
            int resultOk = 0;

            if (_status == SipOperatorStatus.Ready || _status == SipOperatorStatus.NotReady)
            {
                OperatorStatus = _status;
                resultOk = 1;
            }
            
            return resultOk;
        }

        //GDA001, maxy@columbusglobal.com, 24.05.2018
        public string GetOperatorStatus()
        {
            int activeCallsCount = 0;

            //refresh operator status
            if (OperatorStatus != SipOperatorStatus.NotReady)
            {
                //activeCallsCount = ua.CallManager.Calls.Where(x => 
                //    x.IsActive && 
                //    x.InviteState != InviteState.Disconnected
                //    ).Count();
                //if (activeCallsCount == 0)
                //{
                //    OperatorStatus = SipOperatorStatus.Ready;
                //}
                //else
                //{
                //    OperatorStatus = SipOperatorStatus.Busy;
                //}
                // PSN 04.06.2018
                activeCallsCount = ua.CallManager.Calls.Count(x => x.IsActive &&
                    x.InviteState != InviteState.Disconnected);
                OperatorStatus = activeCallsCount == 0 ? SipOperatorStatus.Ready : SipOperatorStatus.Busy;
            }

            return OperatorStatus;
        }
    }
}
