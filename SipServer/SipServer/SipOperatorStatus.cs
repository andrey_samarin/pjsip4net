﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//GDA001, maxy, 24.05.2018
namespace SipServer
{
    public static class SipOperatorStatus
    {
        public const string Ready = "Ready";        //Готов к приему звонка
        public const string NotReady = "NotReady";  //Занят - этот статус был установлен вручную с помощью команды SIP_SETOPERATORSTATUS
        public const string Busy = "Busy";          //Занят - на звонке
    }
}
