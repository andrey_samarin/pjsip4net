﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Windows.Forms;

namespace SipServer
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        public void SetState(string pText)
        {
            TextBoxState.Text = pText;
        }

        public void AddLog(string pText)
        {
            ListBoxLog.Items.Add(pText);
            ListBoxLog.TopIndex = ListBoxLog.Items.Count - 1;
        }

        private void ListBoxLog_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
