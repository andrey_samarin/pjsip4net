﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SipServer
{
    public class Program : Form
    {
        static Mutex mutex = new Mutex(true, "{78F4BBD7-BEA6-4AA3-B89A-DD5A6FD75660}");
        static public MainForm MainForm = null;
        static public WebServer WebServer = null;
        public static SipLib sipLib = null;

        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;

        public Program()
        {
            trayMenu = new ContextMenu();
            trayMenu.MenuItems.Add("Exit", OnExit);

            trayIcon = new NotifyIcon();
            trayIcon.Text = "SipService";
            trayIcon.Icon = SipServer.Properties.Resources.SipPhone;

            // Add menu to tray icon and show it.
            trayIcon.ContextMenu = trayMenu;
            trayIcon.Visible = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            Visible = false; // Hide form window.
            ShowInTaskbar = false; // Remove from taskbar.
                                   //AssistLogger.WriteInformation("Запуск сервиса.");

            base.OnLoad(e);
        }

        private void OnExit(object sender, EventArgs e)
        {
            //Thread.ResetAbort();
            //Thread.Sleep(0);
            //Thread.CurrentThread.Start();
            //AssistLogger.WriteInformation("Окончание работы сервиса.");

            WebServer = null;
            Application.Exit();
        }

        protected override void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                // Release the icon resource.
                trayIcon.Dispose();
            }

            base.Dispose(isDisposing);
        }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (mutex.WaitOne(TimeSpan.Zero, true))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //MainForm = new MainForm();

                WebServer = new WebServer();

                Application.Run(new Program());
                //Thread.Sleep(-1);
                //Thread.Sleep(Timeout.Infinite);
            }
            else
            {
                MessageBox.Show("Only one instance at a time");
            }
        }
    }
}
