﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.ActiveDirectory;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Cache;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Sockets;
using System.ServiceModel.Activation;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Timers;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Serialization;
//using AutoMapper;
//using Columbus.Common;
using Magnum.Extensions;
using pjsip4net.Core.Data;
//using Columbus.Common.Logger;

namespace SipServer
{
    public class CommandRequestDC
    {
        public string сommand = "";
        public string param1 = "";
        public string param2 = "";
        public string param3 = "";
        public string param4 = "";
        public string param5 = "";
    }

    public class CommandResponseDC
    {
        public int resultOk = 0;
        public string result1 = "";
        public string result2 = "";
        public string result3 = "";
        public string result4 = "";
        public string result5 = "";
    }

    public class DeviceInfoResponse
    {
        public int ResultOk = 0;
        public IEnumerable<SoundDeviceInfo> PlayBackDevice { get; set; }
        public IEnumerable<SoundDeviceInfo> CaptureBackDevice { get; set; }
    }

    public class RemoteLinkDC
    {
        public string MachineName = "";
        public int TcpPort = 0;
        public string MachineDomain = "";
    }

    class RemoteLinkDCComparer : IEqualityComparer<RemoteLinkDC>
    {
        public bool Equals(RemoteLinkDC x, RemoteLinkDC y)
        {
            return (x.MachineName == y.MachineName) && (x.TcpPort == y.TcpPort) && (x.MachineDomain == y.MachineDomain);
        }

        public int GetHashCode(RemoteLinkDC obj)
        {
            unchecked
            {
                return (obj.MachineName+obj.MachineDomain+ obj.TcpPort.ToString()).GetHashCode();
            }
        }
    }

    public class LoginUserInfo
    {
        public string UserId { get; set; }
        public string Fullname { get; set; }
        public string OktellLogin { get; set; }
        public string OktellPassw { get; set; }
        public int Status { get; set; }
        public string MessageText { get; set; }
        public DateTime Date { get; set; }
        public string SipLogin { get; set; }
        public string SipPassw { get; set; }
        public string MachineName { get; set; }
        public string TcpPort { get; set; }
        public string MachineDomain { get; set; }
        public string ServiceAddr { get; set; }
        public string PlaybackNum { get; set; }
        public string CaptureNum { get; set; }
        public DateTime? ServiceRegister { get; set; }
    }

    public class WebServer
    {
        public int LocalPort { get; set; }
        public string LocalMachineName { get; set; }
        public string LocalMachineDomain { get; set; }
        //public SipServer.AxSipLib sipLib { get; set; }

        private ServiceHost host;
        private HashSet<RemoteLinkDC> remoteLinks;
        private static System.Timers.Timer aTimer;
        //private DateTime lastRegistrationDateTime;
    
        public CommandResponseDC CallCommandOne(string url, CommandRequestDC request)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            CommandResponseDC ret = new CommandResponseDC();

            try
            {
                req.Headers.Add("SOAPAction", "http://tempuri.org/Service/perform");
                req.ContentType = @"text/xml;charset=""utf-8""";
                req.Accept = "text/xml";
                req.Method = "POST";

                string soapStr = string.Format(
@"<x:Envelope xmlns:x=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"" xmlns:axc=""http://schemas.datacontract.org/2004/07/AxClientServer_CMF"">
    <x:Header/>
    <x:Body>
        <tem:perform>
            <tem:request>
                <axc:command>{0}</axc:command>
                <axc:param1>{1}</axc:param1>
                <axc:param2>{2}</axc:param2>
                <axc:param3>{3}</axc:param3>
                <axc:param4>{4}</axc:param4>
                <axc:param5>{5}</axc:param5>
            </tem:request>
        </tem:perform>
    </x:Body>
</x:Envelope>",
                    request.сommand, //0
                    request.param1, //1
                    request.param2, //2
                    request.param3, //3
                    request.param4, //4
                    request.param5 //5
                    );
                string postData = soapStr;
                byte[] postBytes = Encoding.UTF8.GetBytes(postData);

                req.ContentLength = postBytes.Length;
                Stream requestStream = req.GetRequestStream();
                requestStream.Write(postBytes, 0, postBytes.Length);
                requestStream.Close();

                Stream stream = req.GetResponse().GetResponseStream();
                StreamReader responseReader = new StreamReader(stream);
                string result = responseReader.ReadToEnd();

                XmlSerializer serializer = new XmlSerializer(typeof(CommandResponseDC));
                //ret = (CommandResponseDC)serializer.Deserialize(  ).CanDeserialize().Deserialize(result);
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.LoadXml(result);
                foreach (XmlNode element in xmlDocument.GetElementsByTagName("a:resultOk"))
                {
                    ret.resultOk = int.Parse(element.InnerText);
                }
                foreach (XmlNode element in xmlDocument.GetElementsByTagName("a:result1"))
                {
                    ret.result1 = element.InnerText;
                }
                foreach (XmlNode element in xmlDocument.GetElementsByTagName("a:result2"))
                {
                    ret.result2 = element.InnerText;
                }
                foreach (XmlNode element in xmlDocument.GetElementsByTagName("a:result3"))
                {
                    ret.result3 = element.InnerText;
                }
                foreach (XmlNode element in xmlDocument.GetElementsByTagName("a:result4"))
                {
                    ret.result4 = element.InnerText;
                }
                foreach (XmlNode element in xmlDocument.GetElementsByTagName("a:result5"))
                {
                    ret.result5 = element.InnerText;
                }
                
                
                responseReader.Close();
            }
            finally 
            {
                /*
                ret = new CommandResponseDC();
                ret.resultOk = 0;
                ret.resultDesc = "Internal request error";
                */
            }

            return ret;
        }

        public CommandResponseDC CallCommand(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();
            HashSet<RemoteLinkDC> newRemoteLinks = new HashSet<RemoteLinkDC>();

            foreach (var remoteLinkDc in remoteLinks)
            {
                string url = string.Format("http://{0}{2}:{1}/Soap", 
                    remoteLinkDc.MachineName, 
                    remoteLinkDc.TcpPort,
                    ((remoteLinkDc.MachineDomain.Length > 0) ? "." + remoteLinkDc.MachineDomain : "")
                    );
                try
                {
                    ret = CallCommandOne(url, request);
                    newRemoteLinks.Add(remoteLinkDc);
                }
                catch
                {
                }
            }
            remoteLinks = newRemoteLinks;
            //Program.mainForm.SetState(string.Format("AX Link: %1", remoteLinks.Count));

            return ret;
        }

        public CommandResponseDC CallCommandRing()
        {
            return CallCommand(new CommandRequestDC() { сommand = "SIP_RING", param1 = Program.sipLib.PhoneNumber });
        }

        public CommandResponseDC CallCommandDisconnected()
        {
            return CallCommand(new CommandRequestDC() { сommand = "SIP_DISCONNECTED", param1 = Program.sipLib.PhoneNumber });
        }

        public CommandResponseDC CallCommandConfirmed()
        {
            return CallCommand(new CommandRequestDC() { сommand = " SIP_CONFIRMED", param1 = Program.sipLib.PhoneNumber });
        }

        private CommandResponseDC SlotCommandRegister(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            RemoteLinkDC remoteLinkDC = new RemoteLinkDC();

            remoteLinkDC.MachineName = request.param1; //COMPUTER-NAME
            remoteLinkDC.TcpPort = int.Parse(request.param2); //PORT
            remoteLinkDC.MachineDomain = request.param3; //DOMAIN
            if (remoteLinks.Contains(remoteLinkDC, new RemoteLinkDCComparer()) == false)
            {
                remoteLinks.Add(remoteLinkDC);
                if (Program.sipLib.ua != null)
                {
                    Program.sipLib.ua.Destroy();
                }
                Program.sipLib.ua = null;
            }

            ret.resultOk = 1;
            ret.result1 = (Program.sipLib.ua != null) ? "1" : "0"; //sip-registered

            return ret;
        }
        private CommandResponseDC SlotCommandSipRegister(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            try
            {
                Program.sipLib.Connect(request.param1, request.param2, request.param3, request.param4);
                string[] aStrings = request.param5.Split(';');
                ret.result1 = Program.sipLib.getAvailPlaybackDevices();
                ret.result2 = Program.sipLib.getAvailCaptureDevices();
                if (int.Parse(aStrings[0]) != 0 && int.Parse(aStrings[1]) != 0)
                {
                    Program.sipLib.setSoundDevices(int.Parse(aStrings[0]), int.Parse(aStrings[1]));
                }
                //Program.sipLib.setSoundDevices(7, 5);
                ret.resultOk = 1;
            }
            catch
            {
                if (Program.sipLib.ua != null)
                {
                    Program.sipLib.ua.Destroy();
                }
                Program.sipLib.ua = null;
                ret.resultOk = 0;
            }

            return ret;
        }

        private CommandResponseDC SlotCommandSipCall(CommandRequestDC request)
        {
            int callId;
            CommandResponseDC ret = new CommandResponseDC();

            callId = Program.sipLib.Call(request.param1);

            ret.resultOk = 1;
            ret.result1 = callId.ToString();

            return ret;
        }

        private CommandResponseDC SlotCommandSipAnswer(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            var isAnswer =  Program.sipLib.Answer();

            ret.resultOk = 1;

            return ret;
        }

        private CommandResponseDC SlotCommandSipSetSoundDevices(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            Program.sipLib.setSoundDevices(int.Parse(request.param1), int.Parse(request.param2));

            ret.resultOk = 1;

            return ret;
        }

        private DeviceInfoResponse SlotCommandGetAvailableDevices(CommandRequestDC request)
        {
            return Program.sipLib.GetAvailPlaybackDevices();
        }

        private CommandResponseDC SlotCommandSipHangup(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            Program.sipLib.Hangup();

            ret.resultOk = 1;

            return ret;
        }

        public CommandResponseDC SlotCommand(CommandRequestDC request)
        {
            CommandResponseDC ret;

            switch (request.сommand)
            {
                case "REGISTER":
                    ret = SlotCommandRegister(request);
                    break;
                case "SIP_REGISTER":
                    ret = SlotCommandSipRegister(request);
                    break;
                case "SIP_CALL":
                    ret = SlotCommandSipCall(request);
                    break;
                case "SIP_ANSWER":
                    ret = SlotCommandSipAnswer(request);
                    break;
                case "SIP_SET_SOUND_DEVICES":
                    ret = SlotCommandSipSetSoundDevices(request);
                    break;
                case "SIP_HANGUP":
                    ret = SlotCommandSipHangup(request);
                    break;
                case "SIP_TRANSFER":
                    ret = SlotCommandSipTransfer(request);
                    break;
                case "SIP_SETOPERATORSTATUS":
                    ret = SlotCommandSipSetOperatorStatus(request);
                    break;
                case "SIP_GETOPERATORSTATUS":
                    ret = SlotCommandSipGetOperatorStatus(request);
                    break;

                default:
                    ret = new CommandResponseDC();
                    ret.resultOk = 0;
                    ret.result1 = "Command not defined";
                    break;
            }

            return ret;
        }

        [ServiceContract]
        public class Service //: IService
        {
            [OperationContract]
            public CommandResponseDC perform(CommandRequestDC request)
            {
                CommandResponseDC ret;

                ret = Program.WebServer.SlotCommand(request);
                
                //ret.resultDesc = SipServer.Properties.Settings.Default.authUrl;

                return ret;
            }

            [OperationContract]
            public DeviceInfoResponse GetAvailPlaybackDevices()
            {
                return Program.sipLib.GetAvailPlaybackDevices();
            }
        }

        public void SaveInfoInCrm(string machine, string port, string domain, string uri)
        {
            try
            {
                SaveUserInfoAsync(machine, port, domain, uri).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static async Task SaveUserInfoAsync(string machine, string port, string domain, string uri)
        {
            string webApiUriBase = SipServer.Properties.Settings.Default.WebApiUriBase;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(webApiUriBase);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var data = new LoginUserInfo
                {
                    UserId = String.Empty,
                    MachineName = machine,
                    TcpPort = port,
                    MachineDomain = domain,
                    ServiceAddr = uri
                };
                HttpResponseMessage response = await client.PostAsJsonAsync("api/Usd/SaveCurrentUserInfo", data);
                if (!response.IsSuccessStatusCode)
                {
                    // данные о машине пользователя и не сохранены..
                }
            }
        }

        public void CallSvcRegistration()
        {
//            string registrationUrl = SipServer.Properties.Settings.Default.registrationUrl;
//            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(registrationUrl);
//            req.Headers.Add("SOAPAction", "http://tempuri.org/registerLocalServer");
//            req.ContentType = @"text/xml;charset=""utf-8""";
//            req.Accept = "text/xml";
//            req.Method = "POST";

//            string soapStr = string.Format(
//@"<x:Envelope xmlns:x=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:tem=""http://tempuri.org/"">
//    <x:Header/>
//    <x:Body>
//        <tem:registerLocalServer>
//            <tem:request>
//                <tem:ExtensionData/>
//                <tem:DomainName>{2}</tem:DomainName>
//                <tem:ServerName>{0}</tem:ServerName>
//                <tem:TcpPort>{1}</tem:TcpPort>
//            </tem:request>
//        </tem:registerLocalServer>
//    </x:Body>
//</x:Envelope>",
//            LocalMachineName, //0
//            LocalPort, //1
//            LocalMachineDomain //2
//            );
//            string postData = soapStr;
//            byte[] postBytes = Encoding.UTF8.GetBytes(postData);

//            req.ContentLength = postBytes.Length;
//            Stream requestStream = req.GetRequestStream();
//            requestStream.Write(postBytes, 0, postBytes.Length);
//            requestStream.Close();

//            Stream stream = req.GetResponse().GetResponseStream();
//            StreamReader responseReader = new StreamReader(stream);
//            string result = responseReader.ReadToEnd();
//            //lastRegistrationDateTime = DateTime.Now;
//            //Program.mainForm.AddLog(result);
        }


        public int GetFreePort()
        {
            Socket s = null;
            int returnPort = 0;

            for (int port = 9000; port <= 9999; port++)
            {
                try
                {
                    s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    s.Connect("127.0.0.1", port);
                    if (s.Connected == true)
                    {
                        s.Close();
                    }
                    else
                    {
                        s.Close();
                        returnPort = port;
                        break;
                    }
                }
                catch
                {
                    returnPort = port;
                    break;
                }
                finally
                {
                    if (s != null)
                    {
                        s.Close();
                    }
                }
            }

            return returnPort;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            //Program.mainForm.AddLog(string.Format("{0:HH:mm:ss.fff}", e.SignalTime));
            //Program.webServer.CallSvcRegistration();
            //Program.webServer.callCommand(new CommandRequestDC() {сommand = "HELLO", param1 = DateTime.Now.ToLongDateString()});
            aTimer.Enabled = true;
        }

        private void startTimer()
        {
            aTimer = new System.Timers.Timer(SipServer.Properties.Settings.Default.registrationPeriod); //30 seconds
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.AutoReset = false;
            aTimer.Enabled = true;        
        }

        private void SlotSipIncomingCall(object sender, EventArgs e)
        {
            //MessageBox.Show("Новый входящий звонок!");
            this.CallCommandRing();
        }

        private void SlotSipDisconnected(object sender, EventArgs e)
        {
            this.CallCommandDisconnected();
        }

        private void SlotSipConfirmed(object sender, EventArgs e)
        {
            this.CallCommandConfirmed();
        }

        public WebServer()
        {
            remoteLinks = new HashSet<RemoteLinkDC>();
            Program.sipLib = new SipLib();
            Program.sipLib.eventIncomingCall += SlotSipIncomingCall;
            Program.sipLib.eventDisconnected += SlotSipDisconnected;
            Program.sipLib.eventConfirmed += SlotSipConfirmed;

            LocalPort = this.GetFreePort();
            LocalMachineName = Environment.MachineName;

            // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            try
            {
                Domain domain = Domain.GetComputerDomain();
                LocalMachineDomain = domain.Name;
            }
            catch (Exception ex)
            {
                LocalMachineDomain = ""; //"ru.columbusit.com";
            }

            Uri hostUri = new Uri(string.Format("http://{0}{2}:{1}/", LocalMachineName, LocalPort, LocalMachineDomain.Length > 0 ? "." + LocalMachineDomain : ""));

            //host = new ServiceHost(typeof(Service), new Uri(string.Format("http://{0}:{1}/", localMachineName, localPort)));
            host = new ServiceHost(typeof(Service), hostUri);
            ServiceMetadataBehavior metad
                     = host.Description.Behaviors.Find<ServiceMetadataBehavior>();
            if (metad == null)
            {
                metad = new ServiceMetadataBehavior();
                metad.HttpGetEnabled = true;
                host.Description.Behaviors.Add(metad);
            }
            else
            {
                metad.HttpGetEnabled = true;
            }

            try
            {
                ServiceEndpoint endpoint = host.AddServiceEndpoint(typeof (Service), new BasicHttpBinding(), "Soap");
                host.Open();

                //CallSvcRegistration();
                // запишем данные в CRM
                SaveInfoInCrm(LocalMachineName, LocalPort.ToString(), LocalMachineDomain,
                    $"http://{LocalMachineName}{(LocalMachineDomain.Length > 0 ? "." + LocalMachineDomain : "")}:{LocalPort}/Soap");

                startTimer();
                //Program.mainForm.AddLog("Server: On");
            }
            catch (CommunicationException cex)
            {
                //Console.WriteLine("An exception occurred: {0}", cex.Message);
                host.Abort();
                host = null;
                MessageBox.Show("ServiceHost open error");
                //AssistLogger.Log(cex);
            }
            catch
            {
                host.Abort();
                host = null;
                MessageBox.Show("ServiceHost open error");
            }
        }

        ~WebServer()
        {
            Program.sipLib.Disconnect();
            Program.sipLib = null;
            //GC.Collect();
            if (host != null)
            {
                host.Close();
                host = null;
            }
        }

        //GDA001, maxy, 24.05.2018
        private CommandResponseDC SlotCommandSipTransfer(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            ret.resultOk = Program.sipLib.Transfer(request.param1);

            return ret;
        }

        //GDA001, maxy, 24.05.2018
        private CommandResponseDC SlotCommandSipSetOperatorStatus(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            ret.resultOk = Program.sipLib.SetOperatorStatus(request.param1);

            if (ret.resultOk != 1)
            {
                ret.result1 = string.Format("Could not change status to '{0}'. Acceptable statuses: '{1}', '{2}'",
                        request.param1,
                        SipOperatorStatus.Ready,
                        SipOperatorStatus.NotReady);
            }

            return ret;
        }

        //GDA001, maxy, 24.05.2018
        private CommandResponseDC SlotCommandSipGetOperatorStatus(CommandRequestDC request)
        {
            CommandResponseDC ret = new CommandResponseDC();

            ret.result1 = Program.sipLib.GetOperatorStatus();

            ret.resultOk = 1;

            return ret;
        }
    }
}
